<?php

namespace Tapbuy\PspPluginAdyen\Plugin;

use Adyen\Payment\Gateway\Request\OriginDataBuilder;
use Magento\Framework\Webapi\Rest\Request as WebapiRequest;
use Tapbuy\Checkout\Helper\Data as TapbuyHelper;

class OriginDataBuilderPlugin
{
    private $webapiRequest;
    private $tapbuyHelper;

    public function __construct(
        WebapiRequest $webapiRequest,
        TapbuyHelper $tapbuyHelper
    ) {
        $this->webapiRequest = $webapiRequest;
        $this->tapbuyHelper = $tapbuyHelper;
    }

    public function afterBuild(
        OriginDataBuilder $subject,
        array $buildSubject
    ) {
        // Check if Tapbuy is enabled
        $isEnabled = $this->tapbuyHelper->getConfig('tapbuy_checkout/general/enabled');

        if ($isEnabled) {
            // Get the request content
            $requestBody = json_decode($this->webapiRequest->getContent(), true);

            // Check for the Tapbuy header
            if ($this->webapiRequest->getHeader('X-Tapbuy-Call')) {
                if (!empty($requestBody['paymentMethod']['additional_data']['stateData'])) {
                    $stateData = json_decode($requestBody['paymentMethod']['additional_data']['stateData'], true);
                    if (isset($stateData['origin'])) {
                        $buildSubject['body']['origin'] = $stateData['origin'];
                    }
                }
            }
        }

        return $buildSubject;
    }
}
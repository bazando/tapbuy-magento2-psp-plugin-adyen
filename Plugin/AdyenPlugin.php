<?php

namespace Tapbuy\PspPluginAdyen\Plugin;

use Adyen\Payment\Gateway\Request\PaymentDataBuilder;
use Magento\Framework\Webapi\Rest\Request as WebapiRequest;
use Tapbuy\Checkout\Helper\Data as TapbuyHelper;

class AdyenPlugin
{
    private $webapiRequest;
    private $tapbuyHelper;

    public function __construct(
        WebapiRequest $webapiRequest,
        TapbuyHelper $tapbuyHelper
    ) {
        $this->webapiRequest = $webapiRequest;
        $this->tapbuyHelper = $tapbuyHelper;
    }

    public function beforeBuild(
        PaymentDataBuilder $subject,
        array $buildSubject
    ) {
        // Check if Tapbuy is enabled
        $isEnabled = $this->tapbuyHelper->getConfig('tapbuy_checkout/general/enabled');

        if ($isEnabled) {
            // Get the payment data
            /** @var \Magento\Payment\Model\InfoInterface $payment */
            $payment = $buildSubject['payment']->getPayment();
            $additionalData = $payment->getAdditionalInformation();

            // Get the request content
            $requestBody = json_decode($this->webapiRequest->getContent(), true);

            // Check for the Tapbuy header
            if ($this->webapiRequest->getHeader('X-Tapbuy-Call')) {
                // Modify the additional data
                if (isset($requestBody['paymentMethod']['additional_data']['shopperIP'])) {
                    $additionalData['shopperIP'] = $requestBody['paymentMethod']['additional_data']['shopperIP'];
                }
                if (isset($requestBody['paymentMethod']['additional_data']['returnUrl'])) {
                    $additionalData['returnUrl'] = $requestBody['paymentMethod']['additional_data']['returnUrl'];
                }
                if (!empty($requestBody['paymentMethod']['additional_data']['stateData'])) {
                    $stateData = json_decode($requestBody['paymentMethod']['additional_data']['stateData'], true);
                    if (isset($stateData['origin'])) {
                        $additionalData['origin'] = $stateData['origin'];
                    }
                }

                // Update the payment's additional information
                $payment->setAdditionalInformation($additionalData);
            }
        }

        return [$buildSubject];
    }
}
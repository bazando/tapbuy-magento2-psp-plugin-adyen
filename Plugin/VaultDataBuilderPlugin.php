<?php

namespace Tapbuy\PspPluginAdyen\Plugin;

use Magento\Vault\Api\PaymentTokenManagementInterface;
use Magento\Framework\Serialize\Serializer\Json;
use Psr\Log\LoggerInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\DataObject;
use Magento\Vault\Model\Method\Vault;
use Magento\Framework\Webapi\Request as WebapiRequest;

class VaultDataBuilderPlugin
{
    /**
     * @var PaymentTokenManagementInterface
     */
    private $paymentTokenManagement;

    /**
     * @var Json
     */
    private $jsonSerializer;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var WebapiRequest
     */
    private $webapiRequest;

    public function __construct(
        PaymentTokenManagementInterface $paymentTokenManagement,
        Json $jsonSerializer,
        LoggerInterface $logger,
        WebapiRequest $webapiRequest
    ) {
        $this->paymentTokenManagement = $paymentTokenManagement;
        $this->jsonSerializer = $jsonSerializer;
        $this->logger = $logger;
        $this->webapiRequest = $webapiRequest;
    }

    public function beforeAssignData(
        Vault $subject,
        DataObject $data
    ) {
        try {
            // Check if the request contains the 'X-Tapbuy-Call' header
            $isTapbuyCall = $this->webapiRequest->getHeader('X-Tapbuy-Call');

            if (!$isTapbuyCall) {
                // If not a Tapbuy call, do not modify the payment data
                return [$data];
            }

            $additionalData = $data->getData('additional_data') ?: [];

            // Check if public_hash is already set
            if (isset($additionalData['public_hash'])) {
                return [$data];
            }

            // Retrieve the storedPaymentMethodId from the additional data
            if (!isset($additionalData['stateData'])) {
                return [$data];
            }

            $stateData = $this->jsonSerializer->unserialize($additionalData['stateData']);

            if (isset($stateData['paymentMethod']['storedPaymentMethodId'])) {
                $storedPaymentMethodId = $stateData['paymentMethod']['storedPaymentMethodId'];

                // Get the customer ID from the payment object
                /** @var \Magento\Payment\Model\InfoInterface $paymentInfo */
                $paymentInfo = $subject->getInfoInstance();

                // Try to get the order
                $order = $paymentInfo->getOrder();

                if ($order) {
                    $customerId = $order->getCustomerId();
                } else {
                    // If order is not available, get the quote
                    $quote = $paymentInfo->getQuote();

                    if ($quote) {
                        $customerId = $quote->getCustomerId();
                    } else {
                        throw new LocalizedException(__('Unable to retrieve customer ID.'));
                    }
                }

                if (!$customerId) {
                    throw new LocalizedException(__('Unable to retrieve customer ID.'));
                }

                // Retrieve the payment token
                $paymentMethodCode = $subject->getCode(); // Should be 'adyen_cc_vault'
                $originalPaymentMethodCode = str_replace('_vault', '', $paymentMethodCode);

                $paymentToken = $this->paymentTokenManagement->getByGatewayToken(
                    $storedPaymentMethodId,
                    $originalPaymentMethodCode,
                    $customerId
                );

                if ($paymentToken) {
                    $publicHash = $paymentToken->getPublicHash();

                    // Add public_hash to additional data
                    $additionalData['public_hash'] = $publicHash;

                    // Update the data object
                    $data->setData('additional_data', $additionalData);
                } else {
                    throw new LocalizedException(__('The stored payment method is not available.'));
                }
            }
        } catch (\Exception $e) {
            $this->logger->error('Error in VaultDataBuilderPlugin: ' . $e->getMessage());
            throw new LocalizedException(__('Unable to process the payment.'));
        }

        return [$data];
    }
}
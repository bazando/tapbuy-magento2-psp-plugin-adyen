<?php

namespace Tapbuy\PspPluginAdyen\Plugin;

use Magento\Framework\Webapi\Rest\Request;
use Magento\Vault\Api\Data\PaymentTokenFactoryInterface;
use Magento\Vault\Api\PaymentTokenManagementInterface;
use Magento\Vault\Api\PaymentTokenRepositoryInterface;
use Tapbuy\Checkout\Api\Data\TapbuyCardAlias;

class TapbuyCustomerPlugin
{
    protected $paymentTokenManagement;
    protected $paymentTokenRepository;
    protected $paymentTokenFactory;
    protected $_request;

    public function __construct(
        PaymentTokenManagementInterface $paymentTokenManagement,
        PaymentTokenRepositoryInterface $paymentTokenRepository,
        PaymentTokenFactoryInterface $paymentTokenFactory,
        Request $request
    ) {
        $this->paymentTokenManagement = $paymentTokenManagement;
        $this->paymentTokenRepository = $paymentTokenRepository;
        $this->paymentTokenFactory = $paymentTokenFactory;
        $this->_request = $request;
    }

    private function checkIfPSPRequested(string $psp)
    {
        return $psp === "adyen";
    }

    public function afterGetCardsAliases(\Tapbuy\Checkout\Model\Customer $subject, $result, $customerId, $psp)
    {
        if ($this->checkIfPSPRequested($psp)) {
            $result = $this->getCardsAliases($customerId);
            return $result;
        }
        return $result;
    }

    public function afterAddCardAlias(\Tapbuy\Checkout\Model\Customer $subject, $result, $customerId)
    {
        // Nothing to do - Magento manages the card registration
        return $result;
    }

    public function afterDeleteCardAlias(\Tapbuy\Checkout\Model\Customer $subject, $result, $customerId, $cardAliasId)
    {
        $paymentToken = $this->paymentTokenManagement->getByGatewayToken($cardAliasId, 'adyen_cc', $customerId);
        if ($paymentToken) {
            $paymentToken->setIsVisible(false);
            $this->paymentTokenRepository->save($paymentToken);
        }
        return $this->getCardsAliases($customerId);
    }

    protected function getCardsAliases($customerId)
    {
        $cardList = [];
        $paymentTokens = $this->paymentTokenManagement->getListByCustomerId($customerId);
 
        foreach ($paymentTokens as $token) {
            if ($token->getIsVisible() && $token->getPaymentMethodCode() === 'adyen_cc') {
                $details = json_decode($token->getTokenDetails() ?: '{}', true);
                $expirationDate = isset($details['expirationDate']) ? explode('/', $details['expirationDate']) : ['', ''];
                $pspResponse = [
                    "brand" => $details["type"] ?? '',
                    "expiryMonth" => $expirationDate[0],
                    "expiryYear" => $expirationDate[1],
                    "holderName" => $details["name"] ?? '',
                    "id" => $token->getGatewayToken(),
                    "lastFour" => $details["maskedCC"] ?? '',
                    "type" => "scheme",
                    "storedPaymentMethodId" => $token->getGatewayToken()
                ];
                $alias = new TapbuyCardAlias();
                $alias->setAliasId($token->getEntityId())
                    ->setCustomerId($customerId)
                    ->setAliasName($pspResponse["holderName"])
                    ->setCardType($pspResponse["brand"])
                    ->setCardExpMonth($pspResponse["expiryMonth"])
                    ->setCardExpYear($pspResponse["expiryYear"])
                    ->setCardName($pspResponse["holderName"])
                    ->setCardLast4Digit($pspResponse["lastFour"])
                    ->setCardEncodedNumber("XXXX XXXX XXXX " . $pspResponse["lastFour"])
                    ->setCardToken($pspResponse["id"])
                    ->setPspResponse(json_encode($pspResponse));
                $cardList[] = $alias;
            }
        }
        return $cardList;
    }
}

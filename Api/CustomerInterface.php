<?php

namespace Tapbuy\PspPluginAdyen\Api;

interface CustomerInterface
{

    /**
     * Manage the return of Adyen_cc 3DS Checkout for creating card alias in Magento side
     * @throws InvalidArgumentException
     * @throws InputException
     * @return bool true if alias created, false otherwise
     */
    public function checkAliasCreation();

}

<?php

namespace Tapbuy\PspPluginAdyen\Model;

use Tapbuy\PspPluginAdyen\Api\CustomerInterface;
use Magento\Framework\Webapi\Rest\Request;
use Magento\Sales\Api\OrderRepositoryInterface;
use Magento\Vault\Api\PaymentTokenManagementInterface;
use Magento\Vault\Api\PaymentTokenRepositoryInterface;
use Magento\Vault\Model\PaymentTokenFactory;
use Magento\Framework\Encryption\EncryptorInterface;
use Psr\Log\LoggerInterface;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Framework\ObjectManagerInterface;
use Magento\Vault\Api\Data\PaymentTokenFactoryInterface;

class Customer implements CustomerInterface
{
    protected $_request;
    protected $orderRepository;
    protected $paymentTokenManagement;
    protected $paymentTokenRepository;
    protected $paymentTokenFactory;
    protected $encryptor;
    protected $logger;
    protected $objectManager;
    protected $searchCriteriaBuilder;

    public function __construct(
        Request $request,
        OrderRepositoryInterface $orderRepository,
        PaymentTokenManagementInterface $paymentTokenManagement,
        PaymentTokenRepositoryInterface $paymentTokenRepository,
        PaymentTokenFactory $paymentTokenFactory,
        EncryptorInterface $encryptor,
        LoggerInterface $logger,
        SearchCriteriaBuilder $searchCriteriaBuilder,
        ObjectManagerInterface $objectManager
    ) {
        $this->_request = $request;
        $this->orderRepository = $orderRepository;
        $this->paymentTokenManagement = $paymentTokenManagement;
        $this->paymentTokenRepository = $paymentTokenRepository;
        $this->paymentTokenFactory = $paymentTokenFactory;
        $this->encryptor = $encryptor;
        $this->logger = $logger;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->objectManager = $objectManager;
    }

    public function checkAliasCreation()
    {
        try {
            $bodyParams = $this->_request->getBodyParams();
            if (
                !isset($bodyParams["merchantReference"]) ||
                !isset($bodyParams["additionalData"]) ||
                !isset($bodyParams['additionalData']["recurring.recurringDetailReference"])
            ) {
                return false;
            }

            // Get the order increment ID
            $orderIncrementId = $bodyParams["merchantReference"];

            // Build search criteria to find the order by increment_id
            $searchCriteria = $this->searchCriteriaBuilder
                ->addFilter('increment_id', $orderIncrementId, 'eq')
                ->create();

            // Retrieve the order list
            $orderList = $this->orderRepository->getList($searchCriteria);
            $orders = $orderList->getItems();

            if (empty($orders)) {
                $this->logger->error("Order with increment ID {$orderIncrementId} not found.");
                return false;
            }

            // Get the first order (there should be only one)
            $order = reset($orders);

            // Check if the token already exists
            $customerId = $order->getCustomerId();
            $gatewayToken = $bodyParams['additionalData']["recurring.recurringDetailReference"];

            // Use paymentTokenManagement to get the existing token
            $existingToken = $this->paymentTokenManagement->getByGatewayToken($gatewayToken, 'adyen_cc', $customerId);

            if (!$existingToken) {
                $paymentToken = $this->objectManager->create(
                    \Magento\Vault\Model\PaymentToken::class,
                    ['encryptor' => $this->encryptor]
                );

                // Set token type and other details
                $paymentToken->setType(PaymentTokenFactoryInterface::TOKEN_TYPE_CREDIT_CARD);
                $paymentToken->setGatewayToken($gatewayToken);
                $paymentToken->setCustomerId($customerId);
                $paymentToken->setPaymentMethodCode('adyen_cc');
                $paymentToken->setIsVisible(true);

                // Set token details
                $tokenDetails = [
                    'type' => $bodyParams['additionalData']['paymentMethod'] ?? '',
                    'maskedCC' => $bodyParams['additionalData']['cardSummary'] ?? '',
                    'expirationDate' => $bodyParams['additionalData']['expiryDate'] ?? '',
                    'name' => $bodyParams['additionalData']['cardHolderName'] ?? ''
                ];
                $paymentToken->setTokenDetails(json_encode($tokenDetails));

                // Set expires_at field
                $expiresAt = $this->getExpirationDate($tokenDetails['expirationDate']);
                $paymentToken->setExpiresAt($expiresAt);

                // Generate public_hash
                $paymentToken->getPublicHash();

                // Save the payment token
                $this->paymentTokenRepository->save($paymentToken);

                return true;
            }
        } catch (\Exception $e) {
            $this->logger->error('Error in checkAliasCreation: ' . $e->getMessage());
        }
        return false;
    }

    /**
     * Calculate the expires_at value for the payment token.
     *
     * @param string $expirationDate The card's expiration date in 'MM/YY' format.
     * @return string The expiration date in 'Y-m-d H:i:s' format.
     */
    private function getExpirationDate($expirationDate)
    {
        // Default expiration is 1 year from now
        $defaultExpiration = new \DateTime();
        $defaultExpiration->add(new \DateInterval('P1Y'));
        $defaultExpiration->setTime(0, 0, 0);

        if (!$expirationDate) {
            return $defaultExpiration->format('Y-m-d H:i:s');
        }

        $expDateParts = explode('/', $expirationDate);
        if (count($expDateParts) !== 2) {
            return $defaultExpiration->format('Y-m-d H:i:s');
        }

        $month = str_pad($expDateParts[0], 2, '0', STR_PAD_LEFT);
        $year = $expDateParts[1];

        // Handle 2-digit year to 4-digit year
        if (strlen($year) == 2) {
            $year = '20' . $year;
        }

        // Create a DateTime object for the card's expiration date
        $expiration = \DateTime::createFromFormat('Y-m-d', "$year-$month-01");
        if (!$expiration) {
            return $defaultExpiration->format('Y-m-d H:i:s');
        }

        // Set to the last day of the month at 23:59:59
        $expiration->modify('last day of this month');
        $expiration->setTime(23, 59, 59);

        return $expiration->format('Y-m-d H:i:s');
    }
}